import java.util.Scanner;

public class media {
    public static void main(String[] args) {
        System.out.println("Cálculo de média.");
        Scanner entrada = new Scanner(System.in);
        System.out.print("Informe o número de notas: ");
        int numero_notas = entrada.nextInt();
        double notas[] = new double[numero_notas];
        double soma = 0;
        for (int i = 0; i < numero_notas; i++) {
            System.out.print("Nota " + (i + 1) + ": ");
            notas[i] = entrada.nextDouble();
            soma = soma + notas[i];
        }
        double media = soma / numero_notas;
        System.out.println("A média é: " + media);
    }
}
