import java.util.Scanner;

public class sorteio {
    public static void main(String[] args) {
        long numero = Math.round(Math.random() * 10);
        long chute;
        Scanner entrada = new Scanner(System.in);
        do {
            System.out.print("Informe um número de 1 a 10: ");
            chute = entrada.nextLong();
        } while (numero != chute);
        System.out.println("Acertou!");
    }
}
