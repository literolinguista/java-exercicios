import java.util.Scanner;

public class estacoes {
    public static void main(String[] args) {
	char letra;
	Scanner entrada = new Scanner(System.in);
    System.out.print("Escolha uma das seguintes letras (i, o, p, v): ");
	letra = entrada.nextLine().charAt(0);
	switch (letra) {
		case 'v':
			System.out.println("Verão");
			break;
        case 'p':
            System.out.println("Primavera");
            break;
        case 'o':
            System.out.println("Outono");
            break;
        case 'i':
            System.out.println("Inverno");
            break;
		default:
			System.out.println("Letra inválida!");
    }
}
}
